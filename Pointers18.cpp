// Pointers18.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>



class player
{
private:

    std::string namePlayer;
    int scorePlayer;
    
public:
    
    void Show()
    {
        std::cout << '\n' << namePlayer << ' ' << scorePlayer << std::endl;
    }

    int getScorePlayer()
    {
        return scorePlayer;
    }

    void setPlayer(std::string newNamePlayer, int newScorePlayer)
    {
        namePlayer = newNamePlayer;
        scorePlayer = newScorePlayer;
    }
        
};

// ��� � ���� ����� ������� ����������, � ��� � �� �����. ������, ��� ���� ����� ������������ std::vector.
//void bubbleSort(int* l, int* r)
//{
//    int sz = r - l;
//    if (sz < +1) return;
//    bool b = true;
//    while (b)
//    {
//        b = false;
//        for (int* i = l; i + 1 < r; i++)
//        {
//            if(*i > *(i + 1))
//            {
//                swap(*i, *(i + 1));
//                b = true;
//            }
//        }
//        r--;
//    }
//}

void bubbleSort(player arrForSort[], int size) // � ���� ��� � ����������� ��� ������ ������ player
{
    player buff;


    for (int i = 0; i < size - 1; i++)
    {
        for (int j = size - 1; j > i; j--)
        {
            if (arrForSort[j].getScorePlayer() < arrForSort[j - 1].getScorePlayer())
            {
                buff = arrForSort[j - 1];
                arrForSort[j - 1] = arrForSort[j];
                arrForSort[j] = buff;
            }
        }
    }
}

int main()
{
    std::cout << "How many players do you want?\n";

    int size;
    std::cin >> size;

    player* base = new player[size];

    for (int i = 0; i < size; i++)
    {
        std::string arrName;
        int arrScore;

        std::cout << "Enter " << i+1 << " name:";
        std::cin >> arrName;
        
        std::cout << "Enter " << arrName << " score:";
        std::cin >> arrScore;
        
        base[i].setPlayer(arrName, arrScore);
    }

    std::cout << "\nYou enter: \n";
    for (int i = 0; i < size; i++)
    {
        base[i].Show();
    }

    bubbleSort(base, size);
    
    std::cout << "\nSorted chart: \n";
    for (int i = 0; i < size; i++)
    {
        base[i].Show();
    }
        
    delete[] base;
    base = nullptr;
    return 0;
}

